<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class castfilmController extends Controller
{
    public function create()
    {
        return view('castfilm.create');
    }
 
    public function store(Request $request)
    {
     $request->validate([
         'nama' => 'required|max:255',
         'umur' => 'required',
         'bio' => 'required',
     ]);

     DB::table('castfilm')->insert(
         [
             'nama' => $request['nama'],
             'umur' => $request['umur'],
             'bio' => $request['bio']
         ]

     );

     return redirect('/castfilm');

  
    }

    public function index()
    {
        $castfilm = DB::table('castfilm')->get();

        return view('castfilm.index', compact('castfilm'));
    }


    public function show($id)
    {
        $castfilm = DB::table('castfilm')->where('id', $id)->first();
        
        return view('castfilm.show' , compact('castfilm'));
    }

    public function edit($id)
    {
        $castfilm = DB::table('castfilm')->where('id', $id)->first();
        
        return view('castfilm.edit' , compact('castfilm'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|max:255',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        
        DB::table('castfilm')
            ->where('id', $id)
            ->update(
                [
                    'nama' => $request['nama'],
                    'umur'=> $request['umur'],
                    'bio' => $request['bio']
                ]
            );

        return redirect('/castfilm');
    }

    public function destroy($id)
    {
        DB::table('castfilm')->where('id', '=', $id)->delete();
        return redirect('/castfilm');
    }



 }
 