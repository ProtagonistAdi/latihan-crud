@extends('layout.master')
@section('judul')
    Edit Cast Film {{$castfilm->nama}}
@endsection

@section('content')
<form action="/castfilm/{{$castfilm->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Nama Cast</label>
        <input type="text" class="form-control" value="{{$castfilm->nama}}" name="nama" placeholder="Masukkan Nama Cast">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Umur Cast</label>
        <input type="text" class="form-control" value="{{$castfilm->umur}}" name="umur" placeholder="Masukkan Umur Cast">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Biodata Cast</label>
        <textarea name="bio" id="" class="form-control" cols="30" rows="10">{{$castfilm->bio}}</textarea>
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection