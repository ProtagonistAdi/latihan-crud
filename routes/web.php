<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');
Route::get('/auth', 'AuthController@biodata');
Route::post('/SEND', 'AuthController@send');

Route::get('/data-table', function(){
    return view('table.data-table');
});

// Crud userfilm
// Untuk mengarah ke form tambah castfilm
Route::get('/castfilm/create','castfilmController@create');
// Untuk insert data ke database
Route::post('/castfilm','castfilmController@store');
// Menampilkan semua data pada table castfilm
Route::get('/castfilm','castfilmController@index');
// Menapilkan detail data
Route::get('/castfilm/{castfilm_id}','castfilmController@show');
// Mengarah ke form edit castfilm
Route::get('/castfilm/{castfilm_id}/edit','castfilmController@edit');
// Fungsi Update berdasarkan id
Route::put('/castfilm/{castfilm_id}','castfilmController@update');
// Fungsi delete berdasarkan id
Route::delete('/castfilm/{castfilm_id}', 'castfilmController@destroy');